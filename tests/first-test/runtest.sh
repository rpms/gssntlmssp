#!/bin/bash

# Author: Simo Sorce

. /usr/bin/rhts-environment.sh
echo "rhts-environment sourced, status = $?"

. /usr/share/beakerlib/beakerlib.sh
echo "beakerlib sourced, status = $?"

rlJournalStart

    rlPhaseStartSetup "Check than we have Apache"
        export PACKAGES="httpd mod_auth_gssapi gssntlmssp"
        rlAssertRpm --all
    rlPhaseEnd

    rlPhaseStartSetup "Setup httpd to use mod_auth_gssapi"
        rlRun "mkdir -p /var/www/html"
        rlRun "echo OK > /var/www/html/private"
        rlRun "cp gss.conf /etc/httpd/conf.d/gss.conf"
        rlRun "cp ntlmfile /etc/httpd/ntlmfile"
        rlRun "cp -f httpd.service /etc/systemd/system/httpd.service"
        rlRun "systemctl daemon-reload"
        rlRun "systemctl restart httpd"
    rlPhaseEnd

    rlPhaseStartTest "Run HTTP requests against the setup"
        export NTLM_USER_FILE=/etc/httpd/ntlmfile
        rlRun "curl -si http://$( hostname )/private > /tmp/curl.out.$$"
        rlAssertNotGrep "200 OK" /tmp/curl.out.$$
        rlRun "curl --negotiate -u : -si http://$( hostname )/private > /tmp/curl.out.$$"
        rlAssertGrep "200 OK" /tmp/curl.out.$$
        rlAssertGrep "^OK$" /tmp/curl.out.$$
    rlPhaseEnd

rlJournalEnd
rlJournalPrintText

